package com.mdibrahim.tictactoe;

import java.util.Random;

/**
 * Created by ibrahim007 on 20-Jan-17.
 */
public class TicTacToeGame {

    private char mBoard[];
    private static final int Board_Size=9;

    public static final char Human_player='X';
    public static final char Android_player='0';
    public static final char Empty_space=' ';

    private Random mRand;

    public static int getBoard_Size(){
        return Board_Size;
    }

    public TicTacToeGame(){
        mBoard=new char[Board_Size];
        for (int i=0;i<Board_Size;i++)

        mBoard[i]=Empty_space;
        mRand=new Random();
    }

    public void clearBoard(){
        for (int i=0;i<Board_Size;i++)
        {
            mBoard[i]=Empty_space;
        }
    }

    public void setMove(char player, int location){
        mBoard[location]=player;
    }
    public int getComputerMove()
    {
        int move;
        for (int i=0; i<getBoard_Size();i++)
        {
          if (mBoard[i] !=Human_player && mBoard[i] !=Android_player)
          {
              char curr=mBoard[i];
              mBoard[i]=Android_player;
              if (checkforwinner()==3)
              {
                  setMove(Android_player,i);
                  return i;
              }
              else
                  mBoard[i]=curr;
          }
        }
        for (int i=0; i<getBoard_Size();i++)
        {
            if (mBoard[i] !=Human_player && mBoard[i] !=Android_player)
            {
                char curr=mBoard[i];
                mBoard[i]=Human_player;
                if (checkforwinner()==2)
                {
                    setMove(Android_player,i);
                    return i;
                }
                else
                    mBoard[i]=curr;
            }
        }
        do {
            move=mRand.nextInt(getBoard_Size());
        }
        while (mBoard[move]==Human_player || mBoard[move]==Android_player);
            setMove(Android_player,move);
        return move;
    }

    public int checkforwinner()
    {
        for (int i=0;i<=6; i+=3)
        {
            if (mBoard[i]==Human_player &&
                    mBoard[i+1]==Human_player &&
                    mBoard[i+2]==Human_player)
                return 2;
            if (mBoard[i]==Android_player &&
                    mBoard[i+1]==Android_player &&
                    mBoard[i+2]==Android_player)
                return 3;
        }

        for (int i=0;i<=2;i++)
        {
            if (mBoard[i]==Human_player &&
                    mBoard[i+3]==Human_player &&
                    mBoard[i+6]==Human_player)
                return 2;
            if (mBoard[i]==Android_player &&
                    mBoard[i+3]==Android_player &&
                    mBoard[i+6]==Android_player)
                return 3;
        }

        if ((mBoard[0]==Human_player &&
                mBoard[4]==Human_player &&
                mBoard[8]==Human_player)||
                mBoard[2]==Human_player &&
                 mBoard[4]==Human_player &&
                  mBoard[6]==Human_player)
            return 2;
        if ((mBoard[0]==Android_player &&
                mBoard[4]==Android_player &&
                mBoard[8]==Android_player)||
                mBoard[2]==Android_player &&
                        mBoard[4]==Android_player &&
                        mBoard[6]==Android_player)
            return 3;
        for (int i=0;i<getBoard_Size();i++)
        {
            if (mBoard[i]!=Human_player && mBoard[i]!=Android_player)
                return 0;
        }
        return 1;
    }

}
