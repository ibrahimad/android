package com.example.ibrahim.alartdialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn=(Button) findViewById(R.id.gbtn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder mybulder=new AlertDialog.Builder(MainActivity.this);
                mybulder.setTitle("Attention!");
                mybulder.setMessage("Do you want to Go Search Engine?");
                mybulder.setIcon(R.drawable.attention);

                mybulder.setPositiveButton("I want", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Uri webpage=Uri.parse("http://www.google.com");
                        Intent intent=new Intent(Intent.ACTION_VIEW,webpage);
                        startActivity(intent);
                    }
                });


                mybulder.setNegativeButton("Don't go", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),"i dont wanto to go",Toast.LENGTH_SHORT).show();
                    }
                });

                AlertDialog mydialog=mybulder.create();
                mydialog.show();

            }
        });

    }
}
