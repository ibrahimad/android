package com.mdibrahim.jsonsample;

/**
 * Created by ibrahim007 on 02-Apr-17.
 */

public class User {

    private String name;
    private String hobby;


    public User() {
    }


    public User(String name, String hobby) {
        super();
        this.name = name;
        this.hobby = hobby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

}
