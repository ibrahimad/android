package com.mdibrahim.jsonsample;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ibrahim007 on 02-Apr-17.
 */

public interface APIService {
    @GET("json_bangla")
    Call<List<User>> getUserData();
}
