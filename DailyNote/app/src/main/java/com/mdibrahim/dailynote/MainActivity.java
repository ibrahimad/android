package com.mdibrahim.dailynote;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button savebutton,show;
    EditText headline,description;
    TextView dataview;
    MySqlite my;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        headline=(EditText) findViewById(R.id.headline);
        description=(EditText) findViewById(R.id.description);

        savebutton=(Button) findViewById(R.id.savebtn);
        show=(Button) findViewById(R.id.showbtn);
        dataview=(TextView)findViewById(R.id.view);


        my=new MySqlite(this);


        dataview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(MainActivity.this, "you long touch", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                        MainActivity.this);

// Setting Dialog Title
                alertDialog2.setTitle("Confirm Delete...");

// Setting Dialog Message
                alertDialog2.setMessage("Are you sure you want delete this file?");

// Setting Icon to Dialog
                alertDialog2.setIcon(R.drawable.delete);

// Setting Positive "Yes" Btn
                alertDialog2.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                MainActivity obj=new MainActivity();

                                // Write your code here to execute after dialog
                                Toast.makeText(getApplicationContext(),
                                        "You clicked on YES", Toast.LENGTH_SHORT)

                                        .show();

                            }
                        });

// Setting Negative "NO" Btn
                alertDialog2.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                Toast.makeText(getApplicationContext(),
                                        "You clicked on NO", Toast.LENGTH_SHORT)
                                        .show();
                                dialog.cancel();
                            }
                        });

// Showing Alert Dialog
                alertDialog2.show();
                return true;
            }
        });




        savebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               boolean checker= my.addData(headline.getText().toString(),description.getText().toString());
                if (checker==true){
                    Toast.makeText(MainActivity.this, "Successfuly added", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MainActivity.this, "Not added", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void show(View v) {


        Cursor result=my.showData();
        if (result.getCount()==0){
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            return;
        }
        result.moveToFirst();
        StringBuffer buffer=new StringBuffer();
        do {
            buffer.append(""+result.getString(0)+"\n");
            buffer.append(""+result.getString(1)+"\n\n");

        }while (result.moveToNext());
        showData(buffer.toString());
    }
    public void showData(String data){
        dataview.setText(data);
    }



    public void deletedata(String h){
        my.deletedata(h);
    }



}


