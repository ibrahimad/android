package com.mdibrahim.dailynote;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    TextView textView;
    ListView listView;
    MySqlite my;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        textView=(TextView)findViewById(R.id.dataTextview);
        listView=(ListView)findViewById(R.id.Rlistview);


        my=new MySqlite(this);
    }
    public void show(View v) {


        Cursor result=my.showData();
        if (result.getCount()==0){
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            return;
        }
        result.moveToFirst();
        StringBuffer buffer=new StringBuffer();
        do {
            buffer.append(""+result.getString(0)+"\n");
            buffer.append(""+result.getString(1)+"\n\n");

        }while (result.moveToNext());
        showData(buffer.toString());
    }
    public void showData(String data){
        textView.setText(data);
    }
}


