package com.mdibrahim.dailynote;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ibrahim007 on 01-Mar-17.
 */

public class MySqlite extends SQLiteOpenHelper {


    private static final String DATABASE_NAME="MyDb.db";
    private static final String TABLE_NAME="mytable";

    private static final String COLUMN1="HEADLINE";
    private static final String COLUMN2="DESCRIPTION";


    public MySqlite(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String qr;
        qr="CREATE TABLE "+TABLE_NAME+"("+COLUMN1+" TEXT PRIMARY KEY, "+COLUMN2+" TEXT "+")";
        db.execSQL(qr);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS"+TABLE_NAME);
        onCreate(db);
    }


    public boolean addData(String headline,String desc){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(COLUMN1,headline);
        contentValues.put(COLUMN2,desc);
        long checker=sqLiteDatabase.insert(TABLE_NAME,null,contentValues);
        if (checker==-1){
            return false;
        }else{
            return true;
        }
    }

    public Cursor showData(){
        SQLiteDatabase sqLiteDatabase=getReadableDatabase();
        Cursor cursor;
        cursor=sqLiteDatabase.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        return cursor;
    }

    public void deletedata(String hdln){
        SQLiteDatabase sqLiteDatabase=getReadableDatabase();
        sqLiteDatabase.delete(TABLE_NAME,"HEADLINE = ?",new String[]{hdln});
    }
}
