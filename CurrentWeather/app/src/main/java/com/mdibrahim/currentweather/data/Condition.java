package com.mdibrahim.currentweather.data;

import org.json.JSONObject;

/**
 * Created by ibrahim007 on 20-Mar-17.
 */

public class Condition implements JSONPopulator {
    private int code;
    private int tempareture;
    private String description;

    public int getCode() {
        return code;
    }

    public int getTempareture() {
        return tempareture;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void populate(JSONObject data) {
        code=data.optInt("code");
        tempareture=data.optInt("temp");
        description=data.optString("text");

    }
}
