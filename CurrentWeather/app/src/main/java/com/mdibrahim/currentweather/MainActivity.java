package com.mdibrahim.currentweather;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mdibrahim.currentweather.data.Channel;
import com.mdibrahim.currentweather.data.Item;
import com.mdibrahim.currentweather.service.WeatherServiceCallBack;
import com.mdibrahim.currentweather.service.YahooWeatherService;

public class MainActivity extends AppCompatActivity implements WeatherServiceCallBack {

    private ImageView weatherimageview;
    private TextView conditiontextview,tempareturetextview;

    private YahooWeatherService service;
    private ProgressDialog dialog;
    private Spinner spinner;


    private AdView mAdView;

    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weatherimageview=(ImageView)findViewById(R.id.weathericonimageview);
        tempareturetextview=(TextView)findViewById(R.id.temperaturetextview);
        conditiontextview=(TextView)findViewById(R.id.conditiontextview);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()

                .build();
        mAdView.loadAd(adRequest);





        final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                s=spinner.getSelectedItem().toString();
                //locationtextview.setText(s);
                service.refreshweather(s);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });






        service=new YahooWeatherService(this);

        dialog=new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();








    }



    @Override
    public void serviceSuccess(Channel channel) {
        dialog.hide();

        Item item = channel.getItem();
        int resourceID=getResources().getIdentifier("drawable/icon_" + item.getCondition().getCode(),null,getPackageName());

        @SuppressWarnings("deprecation") Drawable weathericonDrawable=getResources().getDrawable(resourceID);
        weatherimageview.setImageDrawable(weathericonDrawable);

        tempareturetextview.setText(item.getCondition().getTempareture()+"\u00B0"+"F"+channel.getUnits().getTempareture());
        conditiontextview.setText(item.getCondition().getDescription());
        //locationtextview.setText(service.getLocation());
    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(),Toast.LENGTH_LONG).show();

    }


    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

}

