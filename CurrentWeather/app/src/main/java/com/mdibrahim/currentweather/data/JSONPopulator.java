package com.mdibrahim.currentweather.data;

import org.json.JSONObject;

/**
 * Created by ibrahim007 on 20-Mar-17.
 */

public interface JSONPopulator {
    void populate(JSONObject data);
}
