package com.mdibrahim.currentweather.service;

import com.mdibrahim.currentweather.data.Channel;

/**
 * Created by ibrahim007 on 20-Mar-17.
 */

public interface WeatherServiceCallBack {
    void serviceSuccess(Channel channel);
    void serviceFailure(Exception exception);
}
