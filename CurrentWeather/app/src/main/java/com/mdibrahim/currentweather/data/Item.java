package com.mdibrahim.currentweather.data;

import org.json.JSONObject;

/**
 * Created by ibrahim007 on 20-Mar-17.
 */

public class Item implements JSONPopulator {

    private Condition condition;

    public Condition getCondition() {
        return condition;
    }

    @Override
    public void populate(JSONObject data) {
        condition=new Condition();
        condition.populate(data.optJSONObject("condition"));

    }


}
