package com.example.ibrahim.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Ayota on 1/4/2017.
 */

public class FragmentOne extends Fragment {

    View v;
    Button b;
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {

        v=inflater.inflate(R.layout.fragment_one_layout,container,false);
        b=(Button) v.findViewById(R.id.gotomysite);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Go to my site", Toast.LENGTH_SHORT).show();
            }
        });


        return v;
    }
}
