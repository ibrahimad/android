package com.mdibrahim.retrofitsample;

/**
 * Created by ibrahim007 on 03-Apr-17.
 */

public class AndroidVersion {
    private String ver;
    private String name;
    private String api;

    public String getVer() {
        return ver;
    }

    public String getName() {
        return name;
    }

    public String getApi() {
        return api;
    }
}
