package com.mdibrahim.retrofitsample;

/**
 * Created by ibrahim007 on 03-Apr-17.
 */

import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestInterface {

    @GET("android/jsonandroid")
    Call<JSONResponse> getJSON();
}
