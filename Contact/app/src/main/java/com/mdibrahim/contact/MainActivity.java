package com.mdibrahim.contact;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText E1,E2,E3,E4;
    Button B1,B2;
    TextView T1;
    MySqlite my;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        E1=(EditText)findViewById(R.id.editText1);
        E2=(EditText)findViewById(R.id.editText2);
        E3=(EditText)findViewById(R.id.editText3);
        E4=(EditText)findViewById(R.id.editText4);
        B1=(Button) findViewById(R.id.saveButton);
        B2=(Button) findViewById(R.id.showButton);
        T1=(TextView)findViewById(R.id.textview);
        my=new MySqlite(this);


        B1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checker=my.addtotable(E1.getText().toString(),E2.getText().toString(),E3.getText().toString(),E4.getText().toString());
                if (checker==true){
                    Toast.makeText(MainActivity.this, "Successfully inserted", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MainActivity.this, "Data not inserted", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void show(View v){
        Cursor result=my.display();

        if (result.getCount()==0){
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            return;
        }
        result.moveToFirst();

        StringBuffer buffer=new StringBuffer();
        do {

            buffer.append("ID"+result.getString(0)+"\n");
            buffer.append("FIRSTNAME"+result.getString(1)+"\n");
            buffer.append("LASTNAME"+result.getString(2)+"\n");
            buffer.append("EMAIL"+result.getString(3)+"\n\n");

        }while (result.moveToNext());
        display(buffer.toString());

    }
    public void display(String data){
        T1.setText(data);
    }


}
