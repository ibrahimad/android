package com.mdibrahim.contact;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ibrahim007 on 28-Feb-17.
 */

public class MySqlite extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="mydb.db";
    private static final String TABLE_NAME="mytable";
    private static final String COLUMN1="ID";
    private static final String COLUMN2="FIRSTNAME";
    private static final String COLUMN3="LASTNAME";
    private static final String COLUMN4="EMAIL";


    public MySqlite(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query;
        query="CREATE TABLE "+TABLE_NAME+"("+COLUMN1+" INTEGER PRIMARY KEY, "+COLUMN2+" TEXT, "+COLUMN3+" TEXT, "+COLUMN4+" TEXT "+")";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                db.execSQL("DROP TABLE IF EXISTS"+TABLE_NAME);
            onCreate(db);
    }


    public boolean addtotable(String id,String fname,String lname,String email){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();  /** this object use for accessing database to write **/
        ContentValues contentValues=new ContentValues();
        contentValues.put(COLUMN1,id);
        contentValues.put(COLUMN2,fname);
        contentValues.put(COLUMN3,lname);
        contentValues.put(COLUMN4,email);
        long checker=sqLiteDatabase.insert(TABLE_NAME,null,contentValues);

        if (checker==-1){
            return false;
        }
        else {
            return true;
        }

    }

    public Cursor display(){
        SQLiteDatabase sqLiteDatabase=getReadableDatabase();
        Cursor res;
        res=sqLiteDatabase.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        return res;
    }
}
