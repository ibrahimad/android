package com.example.ibrahim.customlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Ayota on 1/2/2017.
 */

public class MyCustomAdapter extends BaseAdapter{

    String[] namesz;
    int[] imsz;
    Context ct;

    private static LayoutInflater inflater=null;

    public MyCustomAdapter(MainActivity mainact,String[] nameofos,int[] osimg){

        namesz=nameofos;
        imsz=osimg;
        ct=mainact;
        inflater=(LayoutInflater) ct.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return namesz.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

   public class Myholder{
        TextView tv;
        ImageView ims;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Myholder myh=new Myholder();
        View myview;

        myview=inflater.inflate(R.layout.custom_layout_item,null);
        myh.tv=(TextView) myview.findViewById(R.id.txtviewid);
        myh.ims=(ImageView) myview.findViewById(R.id.imageviewid);

        myh.tv.setText(namesz[position]);
        myh.ims.setImageResource(imsz[position]);

        myview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ct,"Clicked on "+namesz[position],Toast.LENGTH_LONG).show();
            }
        });


        return myview;
    }
}
