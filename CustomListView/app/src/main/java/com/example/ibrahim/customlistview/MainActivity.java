package com.example.ibrahim.customlistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView mylist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mylist=(ListView) findViewById(R.id.mylistview);


        String[] myos={"Android","iOS","Blackberry","Java","Windows"};
        int[] myosimg={R.drawable.android,R.drawable.apple,R.drawable.blackberry,R.drawable.java,R.drawable.windows};

        MyCustomAdapter myadap=new MyCustomAdapter(this,myos,myosimg);
        mylist.setAdapter(myadap);

    }
}
